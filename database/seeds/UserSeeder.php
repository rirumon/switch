<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'slug' => \Illuminate\Support\Str::slug('Admin'),
            'genders' => 'Other',
            'email' => 'admin@mail.com',
            'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
        ]);
    }
}
