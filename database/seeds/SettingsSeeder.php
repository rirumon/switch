<?php

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            array(
                'name'  =>'default_currencies',
                'value' =>'1'
            ),
            array(
                'name'  =>'type_logo',
                'value' =>'images/logo.svg'
            ),
            array(
                'name'  =>'type_name',
                'value' =>'Master Admin Panel'
            ),
            array(
                'name'  =>'type_footer',
                'value' =>'All Reserve rumon'
            ),
            array(
                'name'  =>'type_mail',
                'value' =>'islamrumon707@gmail.com'
            ),
            array(
                'name'  =>'type_address',
                'value' =>'Voter Golie'
            ),
            array(
                'name'  =>'type_fb',
                'value' =>''
            ),
            array(
                'name'  =>'type_tw',
                'value' =>''
            ),
            array(
                'name'  =>'type_number',
                'value' =>'0987654321'
            ),
            array(
                'name'  =>'type_google',
                'value' =>'google.com'
            ),
            array(
                'name'  =>'footer_logo',
                'value' =>'images/footer_logo.svg'
            ),
            array(
                'name'  =>'favicon_icon',
                'value' =>'images/favicon.ico'
            ),
        ]);
    }
}
