<?php


namespace App\Http\Traits;


use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;

trait LanguageTraits
{
    //list of language
    public function langIndex()
    {
        $languages = Language::all();
        return view('backEnd.common.setting.lang.language',compact('languages'));
    }


    //store a  new language
    public function langStore(Request $request)
    {
        $request->validate([
            'code' => ['required', 'unique:languages'],
            'name' => ['required', 'unique:languages'],
            'image' => ['required', 'unique:languages']
        ]);
        $lan = new Language();
        $lan->code =Str::lower(str_replace(' ','_',$request->code));
        $lan->name = $request->name;
        $lan->image = $request->image;
        $lan->save();
        alert()->success(translate('success'), translate('Language Created Successfully'));
        return back();
    }

    //delete the language
    public function langDestroy($id)
    {
        Language::where('id', $id)->forceDelete();
        alert()->success(translate('success'), translate('Language Delete Successfully'));
        return back();
    }


    //languages for create translate
    public function translate_create($id)
    {
        $lang = Language::findOrFail($id);
        return view('backEnd.common.setting.lang.translate',compact('lang'));
    }


    //translate language save ase a json format file
    public function translate_store(Request $request)
    {
        $language = Language::findOrFail($request->id);

        //check the key have translate data
        $data = openJSONFile($language->code);
        foreach ($request->translations as $key => $value) {
            $data[$key] = $value;
        }

        //save the new keys translate data
        saveJSONFile($language->code, $data);
        alert()->success(translate('success'), translate('Translation has been saved'));
        return back();
    }


    /*languages change in session*/
    public function languagesChange(Request $request)
    {

        session(['locale' => $request->code]);
        Artisan::call('optimize:clear');
        return back();
    }

    //defaultLanguage
    public function defaultLanguage($id)
    {
        $language = Language::findOrFail($id);
        overWriteEnvFile('DEFAULT_LANGUAGE', $language->code);
        alert()->success(translate('success'), translate('Language  Is  Default or changed'));
        return back();
    }
}
