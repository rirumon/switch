@extends('layouts.main')
@section('title')
    @translate(User List)
@endsection
@section('meta-desc')
    @translate(User List)
@endsection
@section('meta-keys')
    @translate(User List)
@endsection
@section('main-content')
    <div class="contentbar">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">@translate(User List)</h3>

                <div class="float-right">
                    <a class="btn btn-success" href="{{ route("users.create") }}">
                        @translate(Add User)
                    </a>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-2">
                <!-- there are the main content-->




            </div>

        </div>
    </div>

@endsection
