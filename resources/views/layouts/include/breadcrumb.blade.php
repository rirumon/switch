
<!-- Start Breadcrumbbar -->
<div class="breadcrumbbar">
    <div class="row align-items-center">
        <div class="col-md-8 col-lg-8">
            <h4 class="page-title">Image Crop</h4>
            <div class="breadcrumb-list">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Advanced UI</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Image Crop</li>
                </ol>
            </div>
        </div>
        <div class="col-md-4 col-lg-4">
            <div class="widgetbar">
                <button class="btn btn-primary-rgba"><i class="feather icon-plus mr-2"></i>Actions</button>
            </div>
        </div>
    </div>
</div>
<!-- End Breadcrumbbar -->

{{--    <div class="container-fluid">--}}
{{--        <div class="row mb-2">--}}
{{--            <div class="col-md-5">--}}
{{--                <h1 class="m-0 text-dark">@yield('title')</h1>--}}
{{--            </div>--}}
{{--            <div class="col-md-6">--}}
{{--                <ol class="breadcrumb float-sm-right">--}}
{{--                    @foreach($segments = request()->segments() as $index=>$segment)--}}
{{--                    <li class="breadcrumb-item">--}}
{{--                        <a href="{{url(implode(array_slice($segments,0,$index+1),'/'))}}">--}}
{{--                            {{Str::title($segment)}}</a></li>--}}
{{--                    @endforeach--}}
{{--                </ol>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}




