<!-- Start Footerbar -->
<div class="footerbar">
    <footer class="footer">
        <p class="mb-0">© {{date('Y')}} {{getSystemSetting('type_name')}} - {{getSystemSetting('type_footer')}}.</p>
    </footer>
</div>
<!-- End Footerbar -->
