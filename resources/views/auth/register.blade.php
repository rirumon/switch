@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="form-head">
                    <a href="{{url('/')}}" class="logo"><img src="{{asset(getSystemSetting('type_logo'))}}"
                                                             class="img-fluid" alt="logo"></a>
                </div>
                <h4 class="text-primary my-4">@translate(Sign Up) !</h4>
                <div class="form-group">
                    <input type="text" class="form-control  @error('name') is-invalid @enderror" name="name"
                           value="{{ old('name') }}" required autocomplete="name" autofocus
                           placeholder="@translate(Enter Username here)">
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                           value="{{ old('email') }}" autocomplete="email" placeholder="@translate(Enter Email here)"
                           required>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password"
                           required autocomplete="new-password" placeholder="@translate(Enter Password here)">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                           autocomplete="new-password" placeholder="@translate(Re-Type Password)" required>
                </div>
                <div class="form-row mb-3 d-none">
                    <div class="col-sm-12">
                        <div class="custom-control custom-checkbox text-left">
                            <input type="checkbox" class="custom-control-input" id="terms">
                            <label class="custom-control-label font-14" for="terms">I Agree to Terms & Conditions of Soyuz</label>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-success btn-lg btn-block font-18">@translate(Register)</button>
            </form>
            <p class="mb-0 mt-3">@translate(Already have an account)?
                <a href="{{route('login')}}">
                    @translate(Log in)
                </a>
            </p>
        </div>
    </div>

@endsection
