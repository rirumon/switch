@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                @csrf
                <div class="form-head">
                    <a href="{{url('/')}}" class="logo"><img src="{{asset(getSystemSetting('type_logo'))}}"
                                                             class="img-fluid" alt="logo"></a>
                </div>

                @if (session('resent'))
                    <div class="alert alert-success" role="alert">
                        @translate(A fresh verification link has been sent to your email address).
                    </div>
                @endif
                <p class="mb-4">@translate(Before proceeding, please check your email for a verification link).</p>
                <p class="mb-4">@translate(If you did not receive the email).</p>
                <button type="submit" class="btn btn-success btn-lg btn-block font-18"> @translate(Click here to request another)</button>.
            </form>
        </div>
    </div>
@endsection
