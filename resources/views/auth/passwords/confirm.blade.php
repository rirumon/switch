@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <form method="POST" action="{{ route('password.confirm') }}">
                @csrf
                <div class="form-head">
                    <a href="{{url('/')}}" class="logo"><img src="{{asset(getSystemSetting('type_logo'))}}"
                                                             class="img-fluid" alt="logo"></a>
                </div>
                <h4 class="text-primary my-4">@translate(Confirm Password) </h4>
                <p class="mb-4">@translate(Please confirm your password before continuing).</p>

                <div class="form-group">

                    <input id="password" type="password" placeholder="@translate(Enter Password)"
                           class="form-control @error('password') is-invalid @enderror" name="password" required
                           autocomplete="current-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror

                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-lg btn-block font-18">
                        @translate(Confirm Password)
                    </button>
                </div>
            </form>
            @if (Route::has('password.request'))
                <p class="mb-0 mt-3"><a href="{{route('password.request')}}">@translate(Forgot Your Password)?</a></p>
            @endif
        </div>
    </div>

@endsection
