@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <form method="POST" action="{{ route('password.email') }}">
                @csrf
                <div class="form-head">
                    <a href="{{url('/')}}" class="logo"><img src="{{asset(getSystemSetting('type_logo'))}}"
                                                             class="img-fluid" alt="logo"></a>
                </div>
                <h4 class="text-primary my-4">@translate(Forgot Password) ?</h4>
                <p class="mb-4">@translate(Enter the email address below to receive reset password instructions).</p>
                <div class="form-group">
                    <input id="email" type="email" placeholder="@translate(Enter Email here)"
                           class="form-control @error('email') is-invalid @enderror" name="email"
                           value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-lg btn-block font-18">
                        @translate(Send Password Reset Link)
                    </button>
                </div>
            </form>
            <p class="mb-0 mt-3">@translate(Remember Password)? <a href="{{route('login')}}">@translate(Log in)</a></p>
        </div>
    </div>
@endsection
