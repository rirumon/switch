@extends('layouts.app')

@section('content')

    <div class="card">

        <div class="card-body">
            <form method="POST" action="{{ route('password.update') }}">
                @csrf
                <div class="form-head">
                    <a href="{{url('/')}}" class="logo"><img src="{{asset(getSystemSetting('type_logo'))}}"
                                                             class="img-fluid" alt="logo"></a>
                </div>
                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group ">
                    <input id="email" type="email" placeholder="@translate(Enter email address)"
                           class="form-control @error('email') is-invalid @enderror" name="email"
                           value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror

                </div>

                <div class="form-group ">
                    <input id="password" type="password" placeholder="@translate(Enter password)"
                           class="form-control @error('password') is-invalid @enderror" name="password" required
                           autocomplete="new-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror

                </div>

                <div class="form-group row">
                    <input placeholder="@translate(Enter Confirm password)" id="password-confirm" type="password"
                           class="form-control" name="password_confirmation" required autocomplete="new-password">
                </div>

                <div class="form-group">

                    <button type="submit" class="btn btn-success btn-lg btn-block font-18">
                        @translate(Reset Password)
                    </button>

                </div>
            </form>
        </div>
    </div>
@endsection
