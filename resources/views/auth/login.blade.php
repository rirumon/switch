@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-head">
                    <a href="{{url('/')}}" class="logo">
                        <img src="{{asset(getSystemSetting('type_logo'))}}" class="img-fluid" alt="logo">
                    </a>
                </div>
                <h4 class="text-primary my-4">@translate(Log in)!</h4>
                <div class="form-group">
                    <input type="email" class="form-control  @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email" autofocus  placeholder="@translate(Enter Email here)" required>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="current-password" placeholder="@translate(Enter Password here)" required>

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="form-row mb-3">
                    <div class="col-sm-6">
                        <div class="custom-control custom-checkbox text-left">
                            <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="custom-control-label font-14" for="remember">@translate(Remember Me)</label>
                        </div>
                    </div>
                    @if (Route::has('password.request'))
                        <div class="col-sm-6">
                            <div class="forgot-psw">
                                <a id="forgot-psw" href="{{ route('password.request') }}" class="font-14">@translate(Forgot Password)?</a>
                            </div>
                        </div>
                    @endif
                </div>
                <button type="submit" class="btn btn-success btn-lg btn-block font-18">@translate(Log in)</button>
            </form>

            <div class="login-or">
                <h6 class="text-muted">@translate(OR)</h6>
            </div>
            <div class="button-list text-center">
                <button type="submit" class="btn btn-primary-rgba font-18"><i class="mdi mdi-facebook mr-2"></i>@translate(Facebook)</button>
                <button type="submit" class="btn btn-danger-rgba font-18"><i class="mdi mdi-google mr-2"></i>@translate(Google)</button>
            </div>
            <p class="mb-0 mt-3">@translate(Don't have a account)? <a href="{{route('register')}}">@translate(Sign up)</a></p>
        </div>
    </div>
@endsection
