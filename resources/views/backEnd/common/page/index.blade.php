@extends('layouts.main')
@section('title') @translate(pages list) @endsection
@section('main-content')
    <!-- Start Contentbar -->
    <div class="contentbar">
        <div class="card">
            <div class="card-header">
                <div class="float-left">
                    <h2 class="card-title">@translate(Page List)</h2>
                </div>
                <div class="float-right">
                    <div class="row">
                        <div class="col">
                            <a href="#!"
                               onclick="forModal('{{ route("pages.create") }}', '@translate(Page Create)')"
                               class="btn btn-primary">
                                <i class="la la-plus"></i>
                                @translate(Page Create)
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <table class="table table-striped- table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>@translate(S/L)</th>
                        <th>@translate(Title)</th>
                        <th>@translate(Total Content)</th>
                        <th>@translate(Active)</th>
                        <th>@translate(Authorize)</th>
                        <th>@translate(Action)</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($pages as  $item)
                        <tr>
                            <td>{{ ($loop->index+1) }}</td>
                            <td>{{checkNull($item->title)}}</td>
                            <td>
                                {{$item->content->count() ?? 'N/A'}}
                            </td>
                            <td>
                                <div class="form-group">
                                    <div
                                        class="custom-control custom-switch">
                                        <input data-id="{{$item->id}}"
                                               {{$item->active == true ? 'checked' : null}}  data-url="{{route('pages.active')}}"
                                               type="checkbox" class="js-switch-primary">

                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                        <input data-id="{{$item->id}}"
                                               {{$item->is_authorize == true ? 'checked' : null}}  data-url="{{route('pages.authorize')}}"
                                               type="checkbox" class="js-switch-danger">
                                </div>
                            </td>
                            <td>
                                <div class="btn-group mr-2">
                                    <div class="dropdown">
                                        <button class="btn btn-round btn-outline-primary" type="button"
                                                id="CustomdropdownMenuButton5" data-toggle="dropdown"
                                                aria-haspopup="true" aria-expanded="false"><i
                                                class="feather icon-more-vertical-"></i></button>
                                        <div class="dropdown-menu" aria-labelledby="CustomdropdownMenuButton5"
                                             x-placement="top-start">
                                            <a class="dropdown-item href"
                                               onclick="forModal('{{ route('pages.edit', $item->id) }}','@translate(Page Edit)')">
                                                <i class="feather icon-edit mr-2"></i>@translate(Edit)</a>

                                            <a class="dropdown-item"
                                               href="{{route('pages.content.index',$item->id)}}"><i
                                                    class="feather icon-file-plus mr-2"></i>@translate(Page Content)</a>

                                            <a class="dropdown-item" href="javascript:void(0)"
                                               onclick="confirm_modal('{{ route('pages.destroy', $item->id) }}')">
                                                <i class="feather icon-delete mr-2"></i>@translate(Delete)</a>

                                        </div>
                                    </div>
                                </div>

                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5"><h3 class="text-center">@translate(No Data Found)</h3></td>
                        </tr>
                    @endforelse
                    </tbody>

                </table>
            </div>
        </div>
    </div>
@endsection

