@extends('layouts.main')
@section('title')
    @translate(User List)
@endsection
@section('main-content')
    <div class="contentbar">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">@translate(User List)</h3>

                <div class="float-right">
                    <a class="btn btn-success" href="{{ route("users.create") }}">
                        @translate(Add User)
                    </a>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-2">
                <!-- there are the main content-->
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>@translate(S/L)</th>
                        <th>@translate(Avatar)</th>
                        <th>@translate(Name)</th>
                        <th>@translate(Contact)</th>
                        <th>@translate(Groups)</th>
                        <th>@translate(Last Login)</th>
                        <th>@translate(Action)</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $item)
                        <tr>
                            <td>
                                {{$loop->index+1}}
                            </td>
                            <td>
                                <img src="{{filePath($item->avatar)}}" width="80" height="80" class="img-circle">
                            </td>
                            <td>@translate(Name) : {{checkNull($item->name)}} <br>
                                <strong>{{checkNull($item->gendear)}}</strong>
                            </td>
                            <td> @translate(Email) : <a href="Mail:{{checkNull($item->email)}}"
                                                        class="text-info">{{checkNull($item->email)}}</a><br>
                                @translate(Phone) : <a href="Tel:{{checkNull($item->tel_number)}}"
                                                       class="text-info">{{checkNull($item->tel_number)}}</a>
                            <td>
                                @foreach($item->groups as $items)
                                    <span class="badge badge-success">{{$items->name}}</span>,
                                @endforeach
                            </td>
                            <td>
                                @if($item->login_time != null)
                                    <span
                                        class="badge badge-info">{{timeForHumans($item->login_time)}}</span>
                                @endif
                            </td>
                            <td>
                                <div class="btn-group mr-2">
                                    <div class="dropdown">
                                        <button class="btn btn-round btn-outline-primary" type="button"
                                                id="CustomdropdownMenuButton5" data-toggle="dropdown"
                                                aria-haspopup="true" aria-expanded="false"><i
                                                class="feather icon-more-vertical-"></i></button>
                                        <div class="dropdown-menu" aria-labelledby="CustomdropdownMenuButton5"
                                             x-placement="top-start">
                                            @if(\Illuminate\Support\Facades\Auth::id() == $item->id)
                                                <a class="dropdown-item" href="{{ route('users.edit') }}">
                                                    <i class="feather icon-user mr-2"></i>@translate(Profile)</a>
                                            @endif
                                            <a class="dropdown-item" href="{{ route('users.show', $item->id) }}"><i
                                                    class="feather icon-eye mr-2"></i>@translate(Show)</a>
                                            @if(\Illuminate\Support\Facades\Auth::id() != $item->id)
                                                <a class="dropdown-item" href="javascript:void(0)"
                                                   onclick="confirm_modal('{{ route('users.banned',$item->id) }}')">
                                                    <i class="feather icon-delete mr-2"></i>@translate(Banned)</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>

@endsection
