@extends('layouts.main')
@section('title') @translate(Module List) @endsection
@section('main-content')
    <div class="contentbar">
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h2 class="card-title">@translate(Module Permission List)</h2>
                </div>
                <div class="card-body">
                    <!--begin: Datatable -->
                    <table class="table data-table">
                        <thead>
                        <tr>
                            <th>@translate(S/L)</th>
                            <th>@translate(Name)</th>
                            <th>@translate(Permissions)</th>
                            <th>@translate(Action)</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($modules as $item)
                            <tr>
                                <td>{{$loop->index+1}}</td>
                                <td>{{checkNull($item->name)}}</td>
                                <td>
                                    @forelse($item->permissions as $permission)
                                        <snap class="badge badge-info">{{$permission->permission->name}}</snap>
                                    @empty
                                        <span class="badge badge-danger">@translate(No permission)</span>
                                    @endforelse
                                </td>
                                <td>
                                    <div class="btn-group mr-2">
                                        <div class="dropdown">
                                            <button class="btn btn-round btn-outline-primary" type="button"
                                                    id="CustomdropdownMenuButton5" data-toggle="dropdown"
                                                    aria-haspopup="true" aria-expanded="false"><i
                                                    class="feather icon-more-vertical-"></i></button>
                                            <div class="dropdown-menu" aria-labelledby="CustomdropdownMenuButton5"
                                                 x-placement="top-start">
                                                <a class="dropdown-item" href="javascript:void(0)" onclick="forModal('{{route('modules.edit',$item->id)}}','Module permission edit')"><i
                                                        class="feather icon-edit mr-2"></i>@translate(Edit)</a>

                                                <a class="dropdown-item" href="javascript:void(0)"
                                                   onclick="confirm_modal('{{route('modules.destroy',$item->id)}}')">
                                                    <i class="feather icon-delete mr-2"></i>@translate(Delete)</a>

                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <!--end: Datatable -->
                </div>
            </div>

        </div>
        <div class="col-md-6 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">@translate(Module Setup)</h3>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" enctype="multipart/form-data"
                          action="{{ route('modules.store') }}"
                          method="POST">
                        @csrf
                        <div class="form-group">
                            <div class="">
                                <label class="control-label">@translate(Name) <span class="text-danger">*</span></label>
                            </div>
                            <div class="">
                                <input type="text" class="form-control" name="name" required
                                       placeholder="@translate(Ex: For category)">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">@translate(Select permissions) <span
                                    class="text-danger">*</span></label>
                            <div class="">
                                <select class="form-control select2" name="p_id[]" multiple required>
                                    <option value=""></option>
                                    @foreach($permissions as $permission)
                                        <option value="{{$permission->id}}"> {{$permission->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-12 text-right">
                                <button class="btn btn-primary btn-block" type="submit">@translate(Save)
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--there are the main content-->
    </div>

@endsection


