@extends('layouts.main')
@section('title') @translate(Group List) @endsection
@section('main-content')
    <div class="contentbar">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">@translate(Group List)</h3>

                <div class="float-right">
                    <a class="btn btn-success" href="{{ route("groups.create") }}">
                        @translate(Add Group)
                    </a>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-2">

                <!-- there are the main content-->
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>@translate(S/L)</th>
                        <th>@translate(Name)</th>
                        <th>@translate(Permission)</th>
                        <th>@translate(Action)</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($groups as $item)
                        <tr>
                            <td> {{$loop->index+1}}</td>
                            <td>@translate(Name) : {{$item->name}} <br> @translate(Slug) : {{$item->slug}}</td>
                            <td>
                                @foreach($item->permissions as $items)
                                    <span class="badge badge-success">{{$items->name}}</span>,
                                @endforeach
                            </td>
                            <td>
                                <div class="btn-group mr-2">
                                    <div class="dropdown">
                                        <button class="btn btn-round btn-outline-primary" type="button"
                                                id="CustomdropdownMenuButton5" data-toggle="dropdown"
                                                aria-haspopup="true" aria-expanded="false"><i
                                                class="feather icon-more-vertical-"></i></button>
                                        <div class="dropdown-menu" aria-labelledby="CustomdropdownMenuButton5"
                                             x-placement="top-start">
                                                <a class="dropdown-item" href="{{ route('groups.edit', $item->id) }}">
                                                    <i class="feather icon-user mr-2"></i>@translate(Profile)</a>

                                            <a class="dropdown-item" href="{{ route('groups.show', $item->id) }}"><i
                                                    class="feather icon-eye mr-2"></i>@translate(Show)</a>

                                            <a class="dropdown-item" href="javascript:void(0)"
                                               onclick="confirm_modal('{{route('groups.destroy',$item->id)}}')">
                                                <i class="feather icon-delete mr-2"></i>@translate(Delete)</a>

                                        </div>
                                    </div>
                                </div>

                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>

        </div>
    </div>

@endsection
